#include "stdafx.h"
#include "GKiW_Lab3.h"

int main(int argc, char* argv[])
{
    glutInit(&argc, argv);

    glutInitWindowPosition(100, 100);
    glutInitWindowSize(640, 360);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

    glutCreateWindow("GKiW: Lab 3");

    glutDisplayFunc(OnRender);
    glutReshapeFunc(OnReshape);
    glutKeyboardFunc(OnKeyPress);
    glutKeyboardUpFunc(OnKeyUp);
    glutSpecialFunc(OnSpecialKeyPress); // klawisze specjalne (strzalki, F1-F12, PgUp/PgDn, Home, End, Delete, Insert)
    glutSpecialUpFunc(OnSpecialKeyUp);
    glutTimerFunc(17, OnTimer, 0);

    glEnable(GL_DEPTH_TEST);

    // Ustawiamy komponent ambient naszej sceny - wartosc niezalezna od swiatla (warto zresetowac)
    float gl_amb[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, gl_amb);

    glEnable(GL_LIGHTING); // Wlaczenie oswietlenia
    glShadeModel(GL_SMOOTH); // Wybor techniki cieniowania
    glEnable(GL_LIGHT0); // Wlaczenie 0-go zrodla swiatla
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);

    player.pos.x = 0.0f;
    player.pos.y = 0.0f;
    player.pos.z = 8.0f;

    player.dir.x = 0.0f;
    player.dir.y = 0.0f;
    player.dir.z = -1.0f;

    player.speed = 1.50f;

    attenuation.constant = 0.5f;
    attenuation.linear = 0.3f;
    attenuation.quadratic = 0.1f;
    fadeOut = true;
    pointAmb = 1.0f;

    glutMainLoop();

    return 0;
}

#pragma region Obsluga wejscia

bool keystate[256];
bool keystate_special[256];

void OnKeyPress(unsigned char key, int x, int y) {
    if (!keystate[key]) {
        OnKeyDown(key, x, y);
    }
    keystate[key] = true;
}

void OnSpecialKeyPress(int key, int x, int y) {
    if (!keystate_special[key]) {
        OnSpecialKeyDown(key, x, y);
    }
    keystate_special[key] = true;
}

void OnKeyDown(unsigned char key, int x, int y) {
    if (key == 27) {
        glutLeaveMainLoop();
    }
    if (key == 'g')
    {
        if (glIsEnabled(GL_LIGHT0))
            glDisable(GL_LIGHT0);
        else
            glEnable(GL_LIGHT0);
    }
    if (key == 'f')
    {
        if (glIsEnabled(GL_LIGHT1))
            glDisable(GL_LIGHT1);
        else
            glEnable(GL_LIGHT1);
    }
}

void OnSpecialKeyDown(int key, int x, int y) {
}

void OnKeyUp(unsigned char key, int x, int y) {
    keystate[key] = false;
}

void OnSpecialKeyUp(int key, int x, int y) {
    keystate_special[key] = false;
}

#pragma endregion

void OnTimer(int id) {

    glutTimerFunc(17, OnTimer, 0);

    T = glutGet(GLUT_ELAPSED_TIME); // Ile milisekund uplynelo od momentu uruchomienia programu?

#pragma region Ruch kamery

    // Zmiana predkosci gracza jesli wcisniete W/S/A/D
    if (keystate['w']) {
        player.velM = player.speed;
    }
    if (keystate['s']) {
        player.velM = -player.speed;
    }
    if (keystate['a']) {
        player.velS = -player.speed;
    }
    if (keystate['d']) {
        player.velS = player.speed;
    }

    // Obrot kamery
    float phi = atan2(player.dir.z, player.dir.x);
    if (keystate['q']) {
        phi -= .03f;
    }
    if (keystate['e']) {
        phi += .03f;
    }

    player.dir.x = cos(phi);
    player.dir.z = sin(phi);

    // Znalezienie kierunku prostopadlego
    vec3 per;
    per.x = -player.dir.z;
    per.z = player.dir.x;

    // Chodzenie przod/tyl
    player.pos.x += player.dir.x * player.velM * .1f;
    player.pos.y += player.dir.y * player.velM * .1f;
    player.pos.z += player.dir.z * player.velM * .1f;

    // Chodzenie na boki
    player.pos.x += per.x * player.velS * .1f;
    player.pos.z += per.z * player.velS * .1f;

    // Bezwladnosc - w kazdym cyklu maleje predkosc gracza
    player.velM /= 1.2;
    player.velS /= 1.2;

#pragma endregion

#pragma region Ruch swiatla

    if (keystate_special[GLUT_KEY_LEFT]) {
        LightPos.x -= .05f;
    }
    if (keystate_special[GLUT_KEY_RIGHT]) {
        LightPos.x += .05f;
    }
    if (keystate_special[GLUT_KEY_UP]) {
        LightPos.y += .05f;
    }
    if (keystate_special[GLUT_KEY_DOWN]) {
        LightPos.y -= .05f;
    }

#pragma endregion

    float fadeFactor = 1.03f;

    if (fadeOut)
    {
        attenuation.constant /= fadeFactor;
        attenuation.linear /= fadeFactor;
        attenuation.quadratic /= fadeFactor;

        pointAmb /= fadeFactor;

        if (attenuation.constant < 0.05f)
            fadeOut = false;
    }
    else
    {
        attenuation.constant *= fadeFactor;
        attenuation.linear *= fadeFactor;
        attenuation.quadratic *= fadeFactor;

        pointAmb *= fadeFactor;

        if (attenuation.constant > 0.5f)
            fadeOut = true;
    }
}

void OnRender() {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    gluLookAt(
        player.pos.x, player.pos.y, player.pos.z,
        player.pos.x + player.dir.x, player.pos.y + player.dir.y, player.pos.z + player.dir.z,
        0.0f, 1.0f, 0.0f
    );

#pragma region Swiatlo

    // directional light
    float l0_amb[] = { 0.1f, 0.1f, 0.1f, 1.0f };
    float l0_dif[] = { 0.1f, 0.1f, 0.1f, 1.0f };
    float l0_spe[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    float l0_pos[] = { LightPos.x, LightPos.y, 1.0f, 0.0f };
    glLightfv(GL_LIGHT0, GL_AMBIENT, l0_amb);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, l0_dif);
    glLightfv(GL_LIGHT0, GL_SPECULAR, l0_spe);
    glLightfv(GL_LIGHT0, GL_POSITION, l0_pos);

    // spot light
    float l1_amb[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    float l1_dif[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    float l1_spe[] = { 0.5f, 0.5f, 0.5f, 1.0f };
    float l1_pos[] = { player.pos.x, player.pos.y, player.pos.z, 1.0f };
    float l1_dir[] = { player.dir.x, player.dir.y, player.dir.z };
    glLightfv(GL_LIGHT1, GL_AMBIENT, l1_amb);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, l1_dif);
    glLightfv(GL_LIGHT1, GL_SPECULAR, l1_spe);
    glLightfv(GL_LIGHT1, GL_POSITION, l1_pos);
    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, l1_dir);
    glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 45.0f);
    glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 0.5f);
    glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, 0.4f);
    glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.3f);
    glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, 5.0f);

    // point light
    float l2_amb[] = { 1.0f, 0.0f, 0.0f, 1.0f };
    float l2_dif[] = { 1.0f, 0.0f, 0.0f, 1.0f };
    float l2_spe[] = { 1.0f, 0.0f, 0.0f, 1.0f };
    float l2_pos[] = { 5.0f, 1.5f, 2.0f, 1.0f };
    glLightfv(GL_LIGHT2, GL_AMBIENT, l2_amb);
    glLightfv(GL_LIGHT2, GL_DIFFUSE, l2_dif);
    glLightfv(GL_LIGHT2, GL_SPECULAR, l2_spe);
    glLightfv(GL_LIGHT2, GL_POSITION, l2_pos);
    glLightf(GL_LIGHT2, GL_CONSTANT_ATTENUATION, attenuation.constant);
    glLightf(GL_LIGHT2, GL_LINEAR_ATTENUATION, attenuation.linear);
    glLightf(GL_LIGHT2, GL_QUADRATIC_ATTENUATION, attenuation.quadratic);

#pragma endregion

    // squares
    float q_amb[] = { 0.3f, 0.3f, 1.0f, 1.0f };
    float q_dif[] = { 0.3f, 0.3f, 1.0f, 1.0f };
    float q_spe[] = { 0.3f, 0.3f, 1.0f, 1.0f };
    glMaterialfv(GL_FRONT, GL_AMBIENT, q_amb);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, q_dif);
    glMaterialfv(GL_FRONT, GL_SPECULAR, q_spe);
    glMaterialf(GL_FRONT, GL_SHININESS, 10.0f);

    for (int ix = -25; ix <= 25; ix += 2) {
        for (int iz = -25; iz <= 25; iz += 2) {
            glPushMatrix();
            glTranslatef(ix, 0.0f, iz);
            glutSolidCube(0.5f);
            glPopMatrix();
        }
    }

    // spheres
    float m_amb[] = { 0.2f, 1.0f, 0.1f, 1.0f };
    float m_dif[] = { 0.2f, 1.0f, 0.1f, 1.0f };
    float m_spe[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    glMaterialfv(GL_FRONT, GL_AMBIENT, m_amb);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, m_dif);
    glMaterialfv(GL_FRONT, GL_SPECULAR, m_spe);
    glMaterialf(GL_FRONT, GL_SHININESS, 20.0f);

    for (int ix = -25; ix <= 25; ix += 2) {
        for (int iz = -25; iz <= 25; iz += 2) {
            glPushMatrix();
            glTranslatef(ix, 3.0f, iz);
            glutSolidSphere(0.5f, 32, 32);
            glPopMatrix();
        }
    }

    // single sphere
    float s_amb[] = { 0.1f, 0.1f, 0.1f, 1.0f };
    float s_dif[] = { 0.1f, 0.1f, 0.1f, 1.0f };
    float s_spe[] = { 0.3f, 0.3f, 0.3f, 1.0f };
    glMaterialfv(GL_FRONT, GL_AMBIENT, s_amb);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, s_dif);
    glMaterialfv(GL_FRONT, GL_SPECULAR, s_spe);
    glMaterialf(GL_FRONT, GL_SHININESS, 10.0f);
    glPushMatrix();
    glTranslatef(5.0f, 1.5f, 2.0f);
    glutSolidSphere(0.3f, 32, 32);
    glPopMatrix();

    glutSwapBuffers();
    glFlush();
    glutPostRedisplay();

}

void OnReshape(int width, int height) {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, width, height);
    gluPerspective(60.0f, (float)width / height, .01f, 100.0f);
}
